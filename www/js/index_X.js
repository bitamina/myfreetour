/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();	
			
		app.resizeMap();
		
		var map = L.map('map-canvas').setView([41.1434, -8.6127], 9);
		
		//this works, but is online:
		/*
		L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18
		}).addTo(map);
		*/
		
		//TODO build something to fall back to web if not found.
		L.tileLayer('img/mapTiles/{z}/{x}/{y}.png', {
			maxZoom: 17
		}).addTo(map);


		L.marker([41.1434, -8.6127]).addTo(map)
			.bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup();

		var popup = L.popup();

		function onMapClick(e) {
			popup
				.setLatLng(e.latlng)
				.setContent("You clicked the map at " + e.latlng.toString() + " | zoom: " + map.getZoom())
				.openOn(map);
		}
		
		var pointA = new L.LatLng(41.1434, -8.6127);
		var pointB = new L.LatLng(41.1477, -8.5863);
		var pointC = new L.LatLng(41.1487, -8.6078);
		var pointList = [pointA, pointB, pointC];

		var firstpolyline = new L.Polyline(pointList, {
		color: 'red',
		weight: 3,
		opacity: 0.5,
		smoothFactor: 1

		});
		firstpolyline.addTo(map);
		
		////////////////////////////////////
		var startPos;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      startPos = position;
      var lat = startPos.coords.latitude;
      var lng = startPos.coords.longitude;
	  console.log (lat +" "+ lng);
    }, function(error) {
      alert("Error occurred. Error code: " + error.code);
      // error.code can be:
      //   0: unknown error
      //   1: permission denied
      //   2: position unavailable (error response from locaton provider)
      //   3: timed out
    });

    navigator.geolocation.watchPosition(function(position) {
      document.getElementById("currentLat").innerHTML = position.coords.latitude;
      document.getElementById("currentLon").innerHTML = position.coords.longitude;
      document.getElementById("distance").innerHTML =
        calculateDistance(startPos.coords.latitude, startPos.coords.longitude,
                          position.coords.latitude, position.coords.longitude);
    });
  };

		////////////////////////////////////
		
		
		

		map.on('click', onMapClick);
		
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
	resizeMap: function() {
		 $("#map-canvas").height(Math.max(100,$(window).height()-90));// TODO set 
	}
	
	
};

	

	$(window).resize(function() {
		app.resizeMap();
	});
