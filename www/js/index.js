/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
            },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var lat;
        var lng;
        var icon;
        var mypos;
        var circlrad, circleteste;

        app.resizeMap();
        
        var map = L.map('map');
        //.setView([41.1434, -8.6127], 15);

        //this works, but is online:
        /*
		L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18
		}).addTo(map);
		*/

        //TODO build something to fall back to web if not found.
        L.tileLayer('img/mapTiles/{z}/{x}/{y}.png', {
            maxZoom: 17,
            minZoom: 14
        }).addTo(map);

        map.locate({
            setView: true
        });

        var pointA = new L.LatLng(41.1434, -8.6127);
        var pointB = new L.LatLng(41.1477, -8.5863);
        var pointC = new L.LatLng(41.1487, -8.6078);
        var pointList = [pointA, pointB, pointC];

        var firstpolyline = new L.Polyline(pointList, {
            color: 'red',
            weight: 3,
            opacity: 0.3,
            smoothFactor: 1

        });
        firstpolyline.addTo(map);
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*var testeGeoJson={
		  "type": "FeatureCollection",
		  "features": [
			{
			  "type": "Feature",
			  "geometry": {
				"type": "Point",
				"coordinates": [
				  -8.5863, 41.1477
				]
			  },
			  "properties": {
				"Ordem": "193",
				"Eixo": "Leste",
				"Meta": "1L",
				"Municipio": "Petrolândia",
				"Estado": "PE",
				"Nome da Comunidade": "Agrovila 4"
			  }
			},
			{
			  "type": "Feature",
			  "geometry": {
				"type": "Point",
				"coordinates": [
				  41.1487, -8.6078
				]
			  },
			  "properties": {
				"Ordem": "194",
				"Eixo": "Leste",
				"Meta": "1L",
				"Municipio": "Petrolândia / Floresta",
				"Estado": "PE",
				"Nome da Comunidade": "Agrovila 5"
			  }
			}
		  ]
		};
		
		new L.geoJson(testeGeoJson).addTo(map);*/
		
var geojsonLayer = new L.GeoJSON.AJAX("rotas/rota001.json");       
geojsonLayer.addTo(map);
var geojsonfontesLayer = new L.GeoJSON.AJAX("rotas/fontes.geojson");       
geojsonfontesLayer.addTo(map);
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////
        /*	function onLocationFound(e) {
				var radius = e.accuracy / 2;

				icon = L.MakiMarkers.icon({icon: "pitch", color: "#b0b", size: "m"});
				
				L.marker(e.latlng, {icon: icon}).addTo(map)
					.bindPopup("You are within " + radius + " meters from this point").openPopup();

				L.circle(e.latlng, radius).addTo(map);
			}

			function onLocationError(e) {
				alert(e.message);
			}

			map.on('locationfound', onLocationFound);
			map.on('locationerror', onLocationError);

			map.locate({setView: true, maxZoom: 16});*/

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                lat = position.coords.latitude;
                lng = position.coords.longitude;
                //console.log(position);
                icon = L.MakiMarkers.icon({
                    icon: "pitch",
                    color: "#b0b",
                    size: "m"
                });
                mypos = L.marker([lat, lng], {
                    icon: icon
                });
                
                var radius = position.coords.accuracy / 2;
                circlrad = L.circle([lat, lng], radius, {stroke:false});
                circlrad.addTo(map);
                
                mypos.addTo(map).bindPopup("<b>Minha Posição</b><br>Erro: " + radius + " metros");

                

            }, function(error) {
                alert("Error occurred. Error code: " + error.code);
                // error.code can be:
                //   0: unknown error
                //   1: permission denied
                //   2: position unavailable (error response from location provider)
                //   3: timed out
            });

            navigator.geolocation.watchPosition(function(position) {
                newlat = position.coords.latitude;
                newlng = position.coords.longitude;
                console.log("nova posição recebida: " + newlat + "," + newlng);
                //map.removeLayer(marker);                
                mypos.setLatLng([newlat, newlng]);             
                //L.marker([position.coords.latitude , position.coords.longitude], {icon: icon}).addTo(map).bindPopup("<b>Minha Posição</b>").openPopup();
                //var luis = new L.marker([position.coords.latitude , position.coords.longitude], {icon: icon});
                //luis.addTo(map).bindPopup("<b>Minha Posição</b>").openPopup();
                //console.log (luis._leaflet_id);
                var newradius = position.coords.accuracy / 2;
                circlrad.setLatLng([newlat, newlng], newradius);
                mypos.bindPopup("<b>Minha Nova Posição</b><br>Erro: " + newradius + " metros");
            },
            function(error) {
                alert("Error occurred. Error code: " + error.code);
                // error.code can be:
                //   0: unknown error
                //   1: permission denied
                //   2: position unavailable (error response from location provider)
                //   3: timed out
            },
            { enableHighAccuracy: true });

        };

        ////////////////////////////////////
        var popup = L.popup();

        L.marker([41.1434, -8.6127]).addTo(map)
            .bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup();

        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("You clicked the map at " + e.latlng.toString() + " | zoom: " + map.getZoom())
                .openOn(map);
        }

        $( ".luis" ).on( "click", function() {
            map.panTo([newlat, newlng]);
            console.log ("click!! at " + newlat +" , "+ newlng);
          });

        map.on('click', onMapClick);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    resizeMap: function() {
            $("#map").height(Math.max(100,$(window).height()-90));// TODO set 
   }
};

$(window).resize(function() {
		app.resizeMap();
	});
